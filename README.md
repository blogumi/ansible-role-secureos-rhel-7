ansible-role-secureos-rhel-7
=========

SecureOS layer role for configuring gold standard linux configurations.

Requirements
------------

* [Development Environment] - Linux
* [Development Environment] - Docker
* [Development Environment] - Python3

Role Variables
--------------

targetEnviornment: [localhost,AWS,OnPrem]

Dependencies
------------

N/a

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: localhost
      roles:
         - { role: ansible-role-secureos-rhel-7, targetEnviornment: localhost }
